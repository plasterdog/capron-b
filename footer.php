<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Capron_Manufacturing
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
<div class="left-foot"><?php bloginfo( 'name' ); ?>&nbsp;
&copy;&nbsp;<?php $the_year = date("Y"); echo $the_year; ?> </div>
<div class="right-foot"></div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
