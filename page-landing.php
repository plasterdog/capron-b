<?php
/**
 * Template Name:Landing Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Capron_Manufacturing
 */

get_header(); ?>

			<?php
			while ( have_posts() ) : the_post(); ?>
<div id="positioned-content">
<!-- THE FOUR IMAGE SECTION -->			
<?php if(get_field('capron_fourth_image')) {?>	
<div class="four-landing-sections">
		<div class="four-landing-sections-container">
			
			<a href="<?php the_field('capron_first_link_url'); ?>">
			<div class="landing-quarter">
			<img src="<?php the_field('capron_first_image'); ?>" alt="<?php the_field('capron_first_caption'); ?>"/>
			<?php if(get_field('capron_first_caption')) {?>
			<div class="landing-quarter-caption">
			<?php the_field('capron_first_caption'); ?>
			</div>
			<?php }	?>
			</div><!-- ends the landing quarter -->

			</a>

			<a href="<?php the_field('capron_second_link_url'); ?>">
			<div class="landing-quarter">
			<img src="<?php the_field('capron_second_image'); ?>" alt="<?php the_field('capron_second_caption'); ?>"/>
			<?php if(get_field('capron_second_caption')) {?>
			<div class="landing-quarter-caption">
			<?php the_field('capron_second_caption'); ?>
			</div>
			<?php }	?>
			</div><!-- ends the landing quarter -->
			</a>
			
			<a href="<?php the_field('capron_third_link_url'); ?>">
			<div class="landing-quarter">
			<img src="<?php the_field('capron_third_image'); ?>" alt="<?php the_field('capron_third_caption'); ?>"/>
			<?php if(get_field('capron_third_caption')) {?>
			<div class="landing-quarter-caption">
			<?php the_field('capron_third_caption'); ?>
			</div>
			<?php }	?>
			</div><!-- ends the landing quarter -->
			</a>

			<a href="<?php the_field('capron_fourth_link_url'); ?>">
			<div class="landing-quarter">
			<img src="<?php the_field('capron_fourth_image'); ?>" alt="<?php the_field('capron_fourth_caption'); ?>"/>
			<?php if(get_field('capron_fourth_caption')) {?>
			<div class="landing-quarter-caption">
			<?php the_field('capron_fourth_caption'); ?>
			</div>
			<?php }	?>
			</div><!-- ends the landing quarter -->
			</a>
			</div><!-- ends the landing quarter -->
		<div class="clear"></div>	
		</div><!-- ends four-landing-sections-container -->
<div class="clear"></div>	
</div><!--ends the four landing sections -->
			<?php }	?>
			<?php if(!get_field('capron_fourth_image')) {?>

<div class="clear"></div>

<?php }	?>
<?php if(get_field('capron_call_to_action_link_target')) {?>
<div class="four-landing-sections bluecall">
		<div class="call-to-action-container">
		<div class="left-side">
		<a href="<?php the_field('capron_call_to_action_link_target'); ?>"><?php the_field('capron_call_to_action_offer'); ?></a>
		</div><!-- ends left side -->
		<div class="right-side">
		<a href="<?php the_field('capron_call_to_action_link_target'); ?>"><?php the_field('capron_call_to_action_link_label'); ?></a>
		</div><!-- ends right side -->
		</div><!-- ends call to action container -->
<div class="clear"></div>			
</div><!-- ends second instance of four landing sections -->
<?php }	?>
<?php if(!get_field('capron_call_to_action_link_target')) {?>

<?php }	?>



<div id="content" class="site-content">
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">	
<!-- DEFAULT SITE CONTENT -->			
			<div class="constrained-section">
			<?php the_content(); ?>
			</div><!-- ends constrained section -->
<!-- ICON SECTION DISPLAY -->			
<?php
// check if the repeater field has rows of data
if( have_rows('capron_icon_sections') ): ?>
<ul class="icon-section-display">
<!--<h3>Results</h3>-->
<?php 	// loop through the rows of data
    while ( have_rows('capron_icon_sections') ) : the_row(); ?>

	<div class="icon-section-display">
	<li>
	<a href="<?php the_sub_field('capron_icon_section_link'); ?>">
<!-- CONDITIONAL LANGUAGE TO INSERT ICON WHEN SELECTED FROM RADIO BUTTON SELECTION -->
			<?php if (get_sub_field('capron_section_icon') == 'appliances') {?> 
			<i class="fa fa-hdd-o"></i>&nbsp;<i class="fa fa-dashboard"></i>&nbsp;<i class="fa fa-plug"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'sporting-equipment') {  ?> 
			<i class="fa fa-bicycle"></i>&nbsp;<i class="fa fa-futbol-o"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'automotive-aerospace-marine') {  ?> 
			<i class="fa fa-automobile"></i>&nbsp;<i class="fa fa-fighter-jet"></i>&nbsp;<i class="fa fa-anchor"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'food-beverage') {  ?> 
			<i class="fa fa-cutlery"></i>&nbsp;<i class="fa fa-glass"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'medical') { ?>
			<i class="fa fa-stethoscope"></i>&nbsp;<i class="fa fa-medkit"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'furniture-labs-education') {  ?>
			<i class="fa fa-bed"></i>&nbsp;<i class="fa fa-flask"></i>&nbsp;<i class="fa fa-graduation-cap"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'anchor') {  ?>
			<i class="fa fa-anchor"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'automobile') {  ?>
			 <i class="fa fa-automobile"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'cultlery') {  ?>
			<i class="fa fa-cutlery"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'bed') {  ?>
			<i class="fa fa-bed"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'fighter-jet') {  ?>
			<i class="fa fa-fighter-jet"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'electronics') {  ?>
			<i class="fa fa-hdd-o"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'lightbulb') {  ?>
			<i class="fa fa-lightbulb-o"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'medkit') {  ?>
			<i class="fa fa-medkit"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'plug') {  ?>
			<i class="fa fa-plug"></i>
			<?php }
			else if (get_sub_field('capron_section_icon') == 'shield') {  ?>
			<i class="fa fa-shield"></i>
<?php }	?>
<h2 class="icon-section-display">
<?php
 // display a sub field value
        the_sub_field('capron_icon_section_label');?>
</h2>

<?php    endwhile;
else :
    // no rows found
endif;
?>
</a>
</li><!--- ENDS THE ICON SECTION' ITEM(S) -->
</div><!-- ends icon-section-display -->
</ul>
<!-- THE FIRST ROW -->
<?php if(get_field('capron_right_lower_column')) {?>
<div class="clear">
	
			<div class="lower-left-section">
			<?php the_field('capron_left_lower_column'); ?>
			</div><!-- ends lower left container -->

<div class="lower-center-right-combo-container">
			<div class="lower-center-section">
			<?php the_field('capron_center_lower_column'); ?>
			</div><!-- ends lower left container -->
	
	
		<div class="lower-right-section">
		<?php the_field('capron_right_lower_column'); ?>
		</div><!--ends lower right container-->
</div><!--ends center-right-combo -->
</div><!--ends clearing div for three column array -->
<?php }	?>
<?php if(!get_field('capron_right_lower_column')) {?>

<?php }	?>
<!-- THE SECOND ROW -->
<?php if(get_field('capron_second_center_lower_column')) {?>
<div class="clear">
	
			<div class="lower-left-section">
			<?php the_field('capron_second_left_lower_column'); ?>
			</div><!-- ends lower left container -->

<div class="lower-center-right-combo-container">
			<div class="lower-center-section">
			<?php the_field('capron_second_center_lower_column'); ?>
			</div><!-- ends lower left container -->
	
	
		<div class="lower-right-section">
		<?php the_field('capron_second_right_lower_column'); ?>
		</div><!--ends lower right container-->
</div><!--ends center-right-combo -->
</div><!--ends clearing div for three column array -->
<?php }	?>
<?php if(!get_field('capron_second_center_lower_column')) {?>

<?php }	?>

<!-- THE THIRD ROW -->
<?php if(get_field('capron_third_center_lower_column')) {?>
<div class="clear">
	
			<div class="lower-left-section">
			<?php the_field('capron_third_left_lower_column'); ?>
			</div><!-- ends lower left container -->

<div class="lower-center-right-combo-container">
			<div class="lower-center-section">
			<?php the_field('capron_third_center_lower_column'); ?>
			</div><!-- ends lower left container -->
	
	
		<div class="lower-right-section">
		<?php the_field('capron_third_right_lower_column'); ?>
		</div><!--ends lower right container-->

</div><!--ends center-right-combo -->
</div><!--ends clearing div for three column array -->
<?php }	?>
<?php if(!get_field('capron_third_center_lower_column')) {?>

<?php }	?>

<!-- THE FOURTH ROW -->
<?php if(get_field('capron_fourth_center_lower_column')) {?>
<div class="clear">
	
			<div class="lower-left-section">
			<?php the_field('capron_fourth_left_lower_column'); ?>
			</div><!-- ends lower left container -->

<div class="lower-center-right-combo-container">
			<div class="lower-center-section">
			<?php the_field('capron_fourth_center_lower_column'); ?>
			</div><!-- ends lower left container -->
	
	
		<div class="lower-right-section">
		<?php the_field('capron_fourth_right_lower_column'); ?>
		</div><!--ends lower right container-->

</div><!--ends center-right-combo -->
</div><!--ends clearing div for three column array -->
<?php }	?>
<?php if(!get_field('capron_fourthd_center_lower_column')) {?>

<?php }	?>


<!-- THE 2 COLUMN SPLIT -->
<?php if(get_field('capron_right_side_lower_half')) {?>
	<div class="clear">
		<div class="lower-left-section">
		<?php the_field('capron_left_side_lower_half'); ?>
		</div><!-- ends left side -->

		<div class="lower-center-right-combo-container">
		<?php the_field('capron_right_side_lower_half'); ?>
		</div><!-- ends right side -->
	</div><!--ends clearing div for two column array -->
<?php }	?>
<?php if(!get_field('capron_right_side_lower_half')) {?>

<?php }	?>

</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="screen-reader-text">%s</span>', 'capron' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->

			<?php	// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
</div><!--ends positioned content -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
