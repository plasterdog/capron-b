<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Capron_Manufacturing
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/font-awesome/css/font-awesome.min.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'capron' ); ?></a>

	<header id="masthead" class="site-header">
<div class="special-interest">

		
				<ul class="top-social-icons">


					<?php if(get_option('pdog_rqu-1') && get_option('pdog_rqu-1') != '') {?>
					<li><a href="<?php echo get_option('pdog_rqu-1') ?>"><i class="fa fa-file-text"></i>&nbsp;
					<?php echo get_option('pdog_rql-1') ?>
					</a>	</li><?php } ?>	

					<?php if(get_option('pdog_rqu-2') && get_option('pdog_rqu-2') != '') {?>
					<li><a href="<?php echo get_option('pdog_rqu-2') ?>"><i class="fa fa-file-text"></i>&nbsp;
					<?php echo get_option('pdog_rql-2') ?>
					</a>	</li><?php } ?>	

					<?php if(get_option('pdog_rqu-3') && get_option('pdog_rqu-3') != '') {?>
					<li><a href="<?php echo get_option('pdog_rqu-3') ?>"><i class="fa fa-file-text"></i>&nbsp;
					<?php echo get_option('pdog_rql-3') ?>
					</a>	</li><?php } ?>		

					<?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
					<li><i class="fa fa-phone"></i>&nbsp;<?php echo get_option('pdog_phone') ?></li>  <?php } ?>	
					
					<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
					<li><a href="mailto:<?php echo get_option('pdog_email') ?>" target="_blank"><i class="fa fa-envelope-o"></i>
					<?php echo get_option('pdog_email') ?>
					</a>	</li><?php } ?>										

					<?php if(get_option('pdog_facebook') && get_option('pdog_facebook') != '') {?>
					<a href="<?php echo get_option('pdog_facebook') ?>" target="_blank"><li><i class="fa fa-facebook"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_linkedin') && get_option('pdog_linkedin') != '') {?>
					<li><a href="<?php echo get_option('pdog_linkedin') ?>" target="_blank"><i class="fa fa-linkedin"></i></a>	</li><?php } ?>	
						
					<?php if(get_option('pdog_googleplus') && get_option('pdog_googleplus') != '') {?>
					<li><a href="<?php echo get_option('pdog_googleplus') ?>" target="_blank"><i class="fa fa-google-plus"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_twitter') && get_option('pdog_twitter') != '') {?>
					<li><a href="<?php echo get_option('pdog_twitter') ?>" target="_blank"><i class="fa fa-twitter"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_instagram') && get_option('pdog_instagram') != '') {?>
					<li><a href="<?php echo get_option('pdog_instagram') ?>" target="_blank"><i class="fa fa-instagram"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_youtube') && get_option('pdog_youtube') != '') {?>
					<li><a href="<?php echo get_option('pdog_youtube') ?>" target="_blank"><i class="fa fa-youtube-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_vimeo') && get_option('pdog_vimeo') != '') {?>
					<li><a href="<?php echo get_option('pdog_vimeo') ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a>	</li><?php } ?>	
				</ul>
		
<div class="clear"></div>
</div>	


	
		<div class="site-branding">
		
<div class="top-contact-side">
		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'capron' ); ?></button>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</nav><!-- #site-navigation -->

<?php if ( ! is_front_page() ) { ?>
<nav id="secondary-navigation" >
    			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-2',
					'menu_id'        => 'secondary-menu',
				) );
			?>
</nav>
<?php } ?>

</div>

<div class="top-logo-side">
						<?php $header_image = get_header_image();
						if ( ! empty( $header_image ) ) { ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="Capron Manufacturing Company" />
						</a>
						<?php } // if ( ! empty( $header_image ) ) ?>




		</div><!-- .site-branding -->


	</header><!-- #masthead -->

	
