<?php
/**
 * Capron Manufacturing functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Capron_Manufacturing
 */

if ( ! function_exists( 'capron_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function capron_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Capron Manufacturing, use a find and replace
		 * to change 'capron' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'capron', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'capron' ),
		) );
		register_nav_menus( array(
			'menu-2' => esc_html__( 'Secondary', 'capron' ),
		) );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'capron_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'capron_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function capron_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'capron_content_width', 640 );
}
add_action( 'after_setup_theme', 'capron_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function capron_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'capron' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'capron' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'capron_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function capron_scripts() {
	wp_enqueue_style( 'capron-style', get_stylesheet_uri() );

	wp_enqueue_script( 'capron-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'capron-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'capron_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/* JMC- remove html filters from category descriptions*/
$filters = array( 'pre_term_description' );

foreach ( $filters as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}

foreach ( array( 'term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_kses_data' );
}
// JMC - allows shortcodes in widgets
add_filter('widget_text', 'do_shortcode');
/**--- JMC OBSCURES LOGIN FAILURE MESSAGE---*/
     add_filter('login_errors',create_function('$a', "return null;"));

 //JMC remove anchor link from more tag

function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');
//* -JMC-Replace WordPress login logo with your own
add_action('login_head', 'b3m_custom_login_logo');
function b3m_custom_login_logo() {
echo '<style type="text/css">
h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login.png) !important; background-size: 209px 150px !important;height: 150px !important; width: 209px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
.login form { margin-top: 10px !important; }
</style>';
}
// JMC- custom footer message
function modify_footer_admin () {
  echo 'Site design by <a href="http://www.plasterdog.com/">Plasterdog Web Design</a>. ';
  echo 'CMS Powered by<a href="http://WordPress.org"> WordPress </a>!';
}
add_filter('admin_footer_text', 'modify_footer_admin');
// JMC Remove WordPress Widgets from Dashboard Area
function remove_wp_dashboard_widgets(){

    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog (News)
    
}
add_action('wp_dashboard_setup', 'remove_wp_dashboard_widgets');

//Remove  WordPress Welcome Panel
remove_action('welcome_panel', 'wp_welcome_panel');
// JMC - change the standard wordpress greeting
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );

function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );

if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __('Restricted Access Area (logged in editor): %1$s'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';

$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );

}
}

// JMC - COMBINING BOTH CUSTOM WIDGETS INTO A SINGLE WIDGET
add_action('wp_dashboard_setup', 'my_dashboard_widgets');

function my_dashboard_widgets() {
     global $wp_meta_boxes;
     unset(
          $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']
     );

add_meta_box( 'dashboard_custom_feed', 'Welcome to your customized site!', 'dashboard_custom_feed_output', 'dashboard', 'side', 'high' );
}
function dashboard_custom_feed_output() {
     echo '<div class="rss-widget">';
     echo '<p>Your site has been significantly customized and many functions exist only in your theme, so think twice before changing it!</p>
 <p>Have a question? contact Jeff McNear by email: <a href="mailto:jeff@plasterdog.com">here</a>. </p>
<p>Old school? give me a call at: 847/849-7060</p>
<hr/>';
     

     echo '</div>';
}
//JMC allow subscriber to see private posts => http://stackoverflow.com/questions/11407181/wordpress-private-posts
$subrole = get_role( 'subscriber' );
// For private pages
$subrole->add_cap( 'read_private_pages' );
/// For private posts
$subrole->add_cap( 'read_private_posts' );

// JMC show admin bar only for admins and editors => https://digwp.com/2011/04/admin-bar-tricks/#disable-for-non-admins
if (!current_user_can('edit_posts')) {
  add_filter('show_admin_bar', '__return_false');
}
//JMC removes the "private" prefix => http://www.trickspanda.com/2014/03/remove-privateprotected-wordpress-post-titles/
function title_format($content) {
return '%s';
}
add_filter('private_title_format', 'title_format');
add_filter('protected_title_format', 'title_format');

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
	// add your extension to the array
	$existing_mimes['vcf'] = 'text/x-vcard';
	return $existing_mimes;
}
//JMC https://wordpress.org/support/topic/remove-category-from-title/
add_filter( 'get_the_archive_title', 'remove_category_word_from_archive_title' );

function remove_category_word_from_archive_title( $title ) {
    $title = str_replace( 'Category:', '', $title );
    return $title;
}